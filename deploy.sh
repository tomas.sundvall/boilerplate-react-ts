#!/bin/bash

declare -A arguments
for ARGUMENT in "$@"
do
  KEY=$(echo $ARGUMENT | cut -f1 -d=)
  VALUE=$(echo $ARGUMENT | cut -f2 -d=)

  arguments[$KEY]=$VALUE
done

echo -e \
"+------------------------------------------------+\n"\
"| Boilerplate react-typescript project           |\n"\
"+------------------------------------------------+\n"

DESTINATION=${arguments["--dest"]}

echo -e \
"| + Copy files                                   |\n"\

cp -ivr "./.vscode" "$DESTINATION"
cp -ivr "./public" "$DESTINATION"
cp -ivr "./src" "$DESTINATION"
cp -ivr "./tsconfig.json" "$DESTINATION"
cp -ivr "./package.json" "$DESTINATION"
cp -ivr "./.prettierrc.js" "$DESTINATION"
cp -ivr "./.gitignore" "$DESTINATION"
cp -ivr "./.eslintrc.js" "$DESTINATION"

echo -e "\n"\
"| + NPM Install                                   |\n"
(cd $DESTINATION ; npm install)

echo -e "\n"\
"| + Check for outdated dependencies               |\n"
(cd $DESTINATION ; npm outdated)